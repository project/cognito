<?php

namespace Drupal\Tests\cognito\Unit;

use Drupal\cognito\Event\CognitoFormEvent;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Test the cognito form event.
 *
 * @group cognito
 */
class CognitoFormEventTest extends UnitTestCase {

  /**
   * The form state mock object.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->formState = $this->createMock(FormStateInterface::class);
  }

  /**
   * Test the getFormState() method.
   */
  public function testGetFormState() {
    $userAttributes = [];
    $cognitoFormEvent = new CognitoFormEvent($this->formState, $userAttributes);
    $result = $cognitoFormEvent->getFormState();
    $this->assertInstanceOf(FormStateInterface::class, $result);
  }

  /**
   * Test the getUserAttributes() method.
   */
  public function testGetUserAttributes() {
    $userAttributes = [
      ['Name' => 'Tester1', 'Value' => 'value1'],
      ['Name' => 'Tester2', 'Value' => 'value2'],
    ];
    $cognitoFormEvent = new CognitoFormEvent($this->formState, $userAttributes);
    $result = $cognitoFormEvent->getUserAttributes();
    $this->assertArrayEquals($userAttributes, $result);
  }

  /**
   * Test the getUserAttributes() method with empty data.
   */
  public function testGetUserAttributesEmpty() {
    $userAttributes = [];
    $cognitoFormEvent = new CognitoFormEvent($this->formState, $userAttributes);
    $result = $cognitoFormEvent->getUserAttributes();
    $this->assertEmpty($result);
  }

  /**
   * Test the setUserAttributes() method.
   */
  public function testSetUserAttributes() {
    $userAttributes = [];
    $cognitoFormEvent = new CognitoFormEvent($this->formState, $userAttributes);
    $result = $cognitoFormEvent->addUserAttribute('Tester1', 'value1');
    $this->assertInstanceOf(CognitoFormEvent::class, $result);
    $this->assertArrayEquals([['Name' => 'Tester1', 'Value' => 'value1']], $result->getUserAttributes());
  }

  /**
   * Test the setUserAttributes() method with multiple values.
   */
  public function testSetUserAttributesInsert() {
    $userAttributes = [
      ['Name' => 'Tester1', 'Value' => 'value1'],
      ['Name' => 'Tester2', 'Value' => 'value2'],
    ];
    $cognitoFormEvent = new CognitoFormEvent($this->formState, $userAttributes);
    $result = $cognitoFormEvent->addUserAttribute('Tester3', 'value3');
    $expected = [
      ['Name' => 'Tester1', 'Value' => 'value1'],
      ['Name' => 'Tester2', 'Value' => 'value2'],
      ['Name' => 'Tester3', 'Value' => 'value3'],
    ];
    $this->assertArrayEquals($expected, $result->getUserAttributes());
  }

}
