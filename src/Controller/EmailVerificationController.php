<?php

namespace Drupal\cognito\Controller;

use Drupal\cognito\Aws\CognitoInterface;
use Drupal\cognito\CognitoTokenInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\externalauth\ExternalAuthInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Verify the users email.
 */
class EmailVerificationController extends ControllerBase {

  /**
   * The cognito service.
   *
   * @var \Drupal\cognito\Aws\Cognito
   */
  protected $cognito;

  /**
   * The external auth service.
   *
   * @var \Drupal\externalauth\ExternalAuthInterface
   */
  protected $externalAuth;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Cognito token.
   *
   * @var \Drupal\cognito\CognitoTokenInterface
   */
  protected $cognitoToken;

  /**
   * ConfirmationController constructor.
   *
   * @param \Drupal\cognito\Aws\CognitoInterface $cognito
   *   The cognito service.
   * @param \Drupal\cognito\CognitoTokenInterface $cognitoToken
   *   The Cognito token.
   * @param \Drupal\externalauth\ExternalAuthInterface $externalAuth
   *   The external auth service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function __construct(CognitoInterface $cognito, CognitoTokenInterface $cognitoToken, ExternalAuthInterface $externalAuth, EventDispatcherInterface $eventDispatcher) {
    $this->cognito = $cognito;
    $this->externalAuth = $externalAuth;
    $this->eventDispatcher = $eventDispatcher;
    $this->cognitoToken = $cognitoToken;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cognito.aws'),
      $container->get('cognito.token'),
      $container->get('externalauth.externalauth'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Confirms a user.
   *
   * @param string $base64_email
   *   The base64 encoded email.
   * @param string $confirmation_code
   *   The confirmation code.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect.
   */
  public function verify($base64_email, $confirmation_code) {
    $email = base64_decode($base64_email);

    if (!$email || !$confirmation_code) {
      $this->messenger()->addMessage($this->t('Invalid email or confirmation code'), 'warning');
      return new RedirectResponse(Url::fromRoute('user.login')->toString(), 302);
    }

    $result = $this->cognito->verifyUserAttribute('email', $confirmation_code, $this->cognitoToken->getAccessToken());
    if ($result->hasError()) {
      $this->messenger()->addMessage($this->t('Your email could not be verified. @message', [
        '@message' => $result->getError(),
      ]), 'warning');
      return new RedirectResponse(Url::fromRoute('user.login')->toString(), 302);
    }

    $this->messenger()->addMessage($this->t('Your email has been verified'));

    $url = Url::fromRoute('<front>');
    $event = new GenericEvent(NULL, ['url' => $url]);
    $this->eventDispatcher->dispatch('cognito.account_verified_redirect', $event);

    $this->eventDispatcher->dispatch('cognito.email_verified', new GenericEvent(NULL, ['email' => $email]));

    return new RedirectResponse($event->getArgument('url')->toString(), 302);
  }

}
