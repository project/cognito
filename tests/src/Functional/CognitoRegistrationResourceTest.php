<?php

namespace Drupal\Tests\cognito\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Tests\rest\Functional\CookieResourceTestTrait;
use Drupal\Tests\rest\Functional\ResourceTestBase;
use GuzzleHttp\RequestOptions;

/**
 * Test the registration resource.
 *
 * @group cognito
 */
class CognitoRegistrationResourceTest extends ResourceTestBase {

  use CookieResourceTestTrait;

  /**
   * The resource config Id.
   *
   * @var string
   */
  protected static $resourceConfigId = 'cognito_user_registration';

  /**
   * Request format.
   *
   * @var string
   */
  protected static $format = 'json';

  /**
   * The MIME type that corresponds to $format.
   *
   * @var string
   */
  protected static $mimeType = 'application/json';

  /**
   * The serializer service.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'cognito',
    'cognito_tests',
    'comment',
    'rest',
    'serialization',
    'rest_test',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    user_role_grant_permissions(AccountInterface::ANONYMOUS_ROLE, ['restful post cognito_user_registration']);
  }

  /**
   * Test checking a users subscription status.
   */
  public function testRegisterViaResource() {
    $this->provisionResource(['json'], ['cookie'], ['POST']);

    $url = Url::fromUri('internal:/cognito/user/register');
    $url->setOption('query', ['_format' => 'json']);
    $request_options[RequestOptions::HEADERS] = [
      'Content-Type' => static::$mimeType,
    ];
    $response = $this->request('POST', $url, $request_options);
    $result = json_decode($response->getBody(), TRUE);

    // We didn't send username or password.
    $this->assertEquals('No user account data for registration received.', $result['message']);

    // Send a username and password.
    $request_options[RequestOptions::BODY] = $this->serializer->encode([
      'name' => ['value' => 'ben'],
      'mail' => ['value' => 'ben@example.com'],
      'pass' => ['value' => 'letmein'],
      'timezone' => ['value' => 'Australia/Perth'],
    ], static::$format);
    $response = $this->request('POST', $url, $request_options);
    $result = json_decode($response->getBody(), TRUE);

    /** @var \Drupal\user\UserInterface $account */
    $accounts = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['mail' => 'ben@example.com']);
    $account = reset($accounts);

    // Ensure we were able to set additional fields.
    $this->assertEquals('Australia/Perth', $account->getTimeZone());
    $this->assertEquals('ben', $account->getAccountName());

    // Ensure the user was registered.
    $this->assertEquals($account->id(), $result['registered_user']['uid']);
    $this->assertEquals(Url::fromRoute('<front>')->toString(), $result['redirect_url']);
  }

  /**
   * Makes the API request.
   *
   * @param string $method
   *   The HTTP method.
   * @param \Drupal\Core\Url $url
   *   A Url object.
   * @param array $request_options
   *   The request options.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The response.
   */
  protected function request($method, Url $url, array $request_options) {
    $request_options[RequestOptions::HTTP_ERRORS] = FALSE;
    $request_options[RequestOptions::ALLOW_REDIRECTS] = FALSE;
    $request_options = $this->decorateWithXdebugCookie($request_options);
    $client = $this->getSession()->getDriver()->getClient()->getClient();
    return $client->request($method, $url->setAbsolute(TRUE)->toString(), $request_options);
  }

  /**
   * Adds the Xdebug cookie to the request options.
   *
   * @param array $request_options
   *   The request options.
   *
   * @return array
   *   Request options updated with the Xdebug cookie if present.
   */
  protected function decorateWithXdebugCookie(array $request_options) {
    $session = $this->getSession();
    $driver = $session->getDriver();
    if ($driver instanceof BrowserKitDriver) {
      $client = $driver->getClient();
      foreach ($client->getCookieJar()->all() as $cookie) {
        if (isset($request_options[RequestOptions::HEADERS]['Cookie'])) {
          $request_options[RequestOptions::HEADERS]['Cookie'] .= '; ' . $cookie->getName() . '=' . $cookie->getValue();
        }
        else {
          $request_options[RequestOptions::HEADERS]['Cookie'] = $cookie->getName() . '=' . $cookie->getValue();
        }
      }
    }
    return $request_options;
  }


  /**
   * {@inheritdoc}
   */
  protected function setUpAuthorization($method) {}

  /**
   * {@inheritdoc}
   */
  protected function assertNormalizationEdgeCases($method, Url $url, array $request_options) {}

  /**
   * {@inheritdoc}
   */
  protected function getExpectedUnauthorizedAccessCacheability() {}

}
