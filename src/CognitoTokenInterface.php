<?php

namespace Drupal\cognito;

/**
 * Interface CognitoTokenInterface.
 */
interface CognitoTokenInterface {

  /**
   * Stores current user's authentication tokens.
   *
   * @param array $authenticationResult
   *   The cognito authentication result.
   *
   * @return $this
   */
  public function setAuthTokens(array $authenticationResult);

  /**
   * Gets current user's access token. Expired token is refreshed.
   *
   * @return string|null
   *   The access token, or NULL if not found.
   */
  public function getAccessToken();

  /**
   * Gets current user's id token. Expired token is refreshed.
   *
   * @return string|null
   *   The id token, or NULL if not found.
   */
  public function getIdToken();

  /**
   * Admin method to get a user's access token.
   *
   * @param int $uid
   *   The Drupal user id.
   *
   * @return string|null
   *   The access token, or NULL if not found.
   */
  public function adminGetAccessToken($uid);

  /**
   * Admin method to delete a user's authentication tokens.
   *
   * @param int $uid
   *   The Drupal user id.
   *
   * @return $this
   */
  public function adminDeleteAuthTokens($uid);

}
