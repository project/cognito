<?php

namespace Drupal\cognito;

use Drupal\cognito\Aws\CognitoResult;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserDataInterface;
use Drupal\cognito\Aws\CognitoInterface;

/**
 * Class CognitoToken.
 */
class CognitoToken implements CognitoTokenInterface {

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * The cognito service.
   *
   * @var \Drupal\cognito\Aws\CognitoInterface
   */
  protected $cognito;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new CognitoToken object.
   *
   * @param \Drupal\user\UserDataInterface $userData
   *   The user data service.
   * @param \Drupal\cognito\Aws\CognitoInterface $cognito
   *   The cognito service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(UserDataInterface $userData, CognitoInterface $cognito, AccountProxyInterface $currentUser, TimeInterface $time) {
    $this->userData = $userData;
    $this->cognito = $cognito;
    $this->currentUser = $currentUser;
    $this->time = $time;
  }

  /**
   * Stores access token.
   *
   * @param array $authenticationResult
   *   The cognito authentication result.
   *
   * @return $this
   */
  protected function setAccessToken(array $authenticationResult) {
    if (!empty($authenticationResult['AccessToken']) && !empty($authenticationResult['ExpiresIn'])) {
      $accessToken = [
        'token' => $authenticationResult['AccessToken'],
        'expires' => $this->time->getCurrentTime() + $authenticationResult['ExpiresIn'],
      ];
      $this->set('AccessToken', $accessToken);
    }
    return $this;
  }

  /**
   * Stores the Id token.
   *
   * @param array $authenticationResult
   *   The cognito authentication result.
   *
   * @return $this
   */
  protected function setIdToken(array $authenticationResult) {
    if (!empty($authenticationResult['IdToken']) && !empty($authenticationResult['ExpiresIn'])) {
      $idToken = [
        'token' => $authenticationResult['IdToken'],
        'expires' => $this->time->getCurrentTime() + $authenticationResult['ExpiresIn'],
      ];
      $this->set('IdToken', $idToken);
    }
    return $this;
  }

  /**
   * Stores refresh token.
   *
   * @param array $authenticationResult
   *   The cognito authentication result.
   *
   * @return $this
   */
  protected function setRefreshToken(array $authenticationResult) {
    if (!empty($authenticationResult['RefreshToken'])) {
      $this->set('RefreshToken', $authenticationResult['RefreshToken']);
    }
    return $this;
  }

  /**
   * Stores arbitrary data against current user.
   *
   * @param string $key
   *   The name of the data key.
   * @param mixed $data
   *   Data to be stored.
   *
   * @return $this
   */
  protected function set($key, $data) {
    $this->userData->set('cognito', $this->currentUser->id(), $key, $data);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setAuthTokens(array $authenticationResult) {
    // Store access token and refresh token separately.
    $this
      ->setAccessToken($authenticationResult)
      ->setIdToken($authenticationResult)
      ->setRefreshToken($authenticationResult);
    return $this;
  }

  /**
   * Retrieves stored user access token.
   *
   * @param int $uid
   *   The Drupal user id.
   * @param string $tokenType
   *   (optional) Either AccessToken or IdToken
   *
   * @return array|null
   *   The access token keyed 'token' and expiry timestamp keyed 'expires', or
   *   NULL if not found.
   */
  protected function retrieveStoredToken($uid, $tokenType = 'AccessToken') {
    return $this->userData->get('cognito', $uid, $tokenType);
  }

  /**
   * Retrieves stored user refresh token.
   *
   * @param int $uid
   *   The Drupal user id.
   *
   * @return string|null
   *   The refresh token, or NULL if not found.
   */
  protected function retrieveStoredRefreshToken($uid) {
    return $this->userData->get('cognito', $uid, 'RefreshToken');
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken() {
    return $this->retrieveToken('AccessToken');
  }

  /**
   * {@inheritdoc}
   */
  public function getIdToken() {
    return $this->retrieveToken('IdToken');
  }

  /**
   * @param string $tokenType
   *   Must be either AccessToken or IdToken.
   *
   * @return array
   *   The token.
   */
  protected function retrieveToken($tokenType) {
    $tokenString = NULL;
    $uid = $this->currentUser->id();
    if ($token = $this->retrieveStoredToken($uid, $tokenType)) {
      if ($this->time->getCurrentTime() < $token['expires']) {
        // Return active token.
        $tokenString = $token['token'];
      }
      else {
        // Refresh expired token.
        $result = $this->cognito->refreshAccessToken($this->retrieveStoredRefreshToken($uid));
        if ($result instanceof CognitoResult) {
          if (!$result->hasError()) {
            $authenticationResult = $result->getResult()['AuthenticationResult'];
            $token = $this
              ->setAuthTokens($authenticationResult)
              ->retrieveStoredToken($uid, $tokenType);
            $tokenString = $token['token'];
          }
        }
      }
    }
    return $tokenString;
  }

  /**
   * {@inheritdoc}
   */
  public function adminGetAccessToken($uid) {
    $accessToken = $this->retrieveStoredToken($uid);
    $token = !empty($accessToken['token']) ? $accessToken['token'] : NULL;
    return $token;
  }

  /**
   * {@inheritdoc}
   */
  public function adminDeleteAuthTokens($uid) {
    $this->userData->delete('cognito', $uid, 'AccessToken');
    $this->userData->delete('cognito', $uid, 'RefreshToken');
    return $this;
  }

}
