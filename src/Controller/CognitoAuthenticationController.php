<?php

namespace Drupal\cognito\Controller;

use Drupal\user\Controller\UserAuthenticationController;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Controller to login using Cognito.
 */
class CognitoAuthenticationController extends UserAuthenticationController {

  /**
   * Logs in a user.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response which contains the ID and CSRF token.
   */
  public function login(Request $request) {
    $format = $this->getRequestFormat($request);
    /** @var \Drupal\cognito\Aws\Cognito $cognito */
    $cognito = \Drupal::service('cognito.aws');
    $cognitoMessages = \Drupal::service('cognito.messages');
    $externalAuth = \Drupal::service('externalauth.externalauth');
    $eventDispatcher = \Drupal::service('event_dispatcher');

    $content = $request->getContent();
    $credentials = $this->serializer->decode($content, $format);
    if (!isset($credentials['name']) && !isset($credentials['pass'])) {
      throw new BadRequestHttpException('Missing credentials.');
    }

    if (!isset($credentials['name'])) {
      throw new BadRequestHttpException('Missing credentials.name.');
    }
    if (!isset($credentials['pass'])) {
      throw new BadRequestHttpException('Missing credentials.pass.');
    }

    $this->floodControl($request, $credentials['name']);
    if ($this->userIsBlocked($credentials['name'])) {
      throw new BadRequestHttpException('The user has not been activated or is blocked.');
    }

    $result = $cognito->authorize($credentials['name'], $credentials['pass']);
    if ($result->hasError()) {
      $flood_config = $this->config('user.flood');
      if ($identifier = $this->getLoginFloodIdentifier($request, $credentials['name'])) {
        $this->flood->register('user.http_login', $flood_config->get('user_window'), $identifier);
      }
      // Always register an IP-based failed login event.
      $this->flood->register('user.failed_login_ip', $flood_config->get('ip_window'));

      throw new BadRequestHttpException($result->getErrorCode() === 'PasswordResetRequiredException' ? $cognitoMessages->passwordResetRequired() : $result->getError());
    }

    // Logged in successfully.
    $user = $externalAuth->login($credentials['name'], 'cognito');
    if (!$user) {
      throw new BadRequestHttpException('A problem occurred logging you in, please contact an administrator');
    }

    $this->flood->clear('user.http_login', $this->getLoginFloodIdentifier($request, $credentials['name']));

    // Let everyone else know.
    $event = new GenericEvent(NULL, [
      'uid' => $user->id(),
      'login-context' => \Drupal::request()->get('cognito-login-context') ?: [],
    ]);
    $eventDispatcher->dispatch('cognito.logged_in', $event);

    return new Response($this->serializer->encode([
      'uid' => $user->id(),
      'name' => $user->getAccountName(),
    ], $format));
  }

}
