<?php


namespace Drupal\cognito\Plugin\rest\resource;

use Drupal\cognito\CognitoTokenInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provide the Cognito IdTokens as an endpoint.
 *
 * @RestResource(
 *   id = "cognito_auth_token",
 *   label = @Translation("Cognito Auth Token"),
 *   uri_paths = {
 *     "canonical" = "/cognito/auth-token",
 *   },
 * )
 */
class CognitoAuthToken extends ResourceBase {

  /**
   * The Cognito token service.
   *
   * @var \Drupal\cognito\CognitoTokenInterface
   */
  protected $cognitoToken;

  /**
   * CognitoAuthToken constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param $plugin_id
   *   Plugin ID.
   * @param $plugin_definition
   *   Plugin definition.
   * @param array $serializer_formats
   *   Serializer Formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\cognito\CognitoTokenInterface $cognitoToken
   *   Cognito token service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, CognitoTokenInterface $cognitoToken) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->cognitoToken = $cognitoToken;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('cognito.token')
    );
  }

  /**
   * Gets the Cognito auth tokens.
   */
  public function get() {
    return new JsonResponse([
      'idToken' => $this->cognitoToken->getIdToken(),
      'accessToken' => $this->cognitoToken->getAccessToken(),
    ]);
  }

}
