<?php

namespace Drupal\cognito\Form\Email;

use Drupal\cognito\Aws\CognitoInterface;
use Drupal\cognito\Aws\CognitoResult;
use Drupal\cognito\CognitoFlowManagerInterface;
use Drupal\cognito\CognitoMessagesInterface;
use Drupal\cognito\CognitoTokenInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Email;
use Drupal\externalauth\ExternalAuthInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * The user login form.
 */
class UserLoginForm extends FormBase {

  /**
   * The cognito service.
   *
   * @var \Drupal\cognito\Aws\Cognito
   */
  protected $cognito;

  /**
   * The messages service.
   *
   * @var \Drupal\cognito\CognitoMessages
   */
  protected $cognitoMessages;

  /**
   * The external auth service.
   *
   * @var \Drupal\externalauth\ExternalAuthInterface
   */
  protected $externalAuth;

  /**
   * The cognito flow manager.
   *
   * @var \Drupal\cognito\CognitoFlowManagerInterface
   */
  protected $cognitoFlowManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The cognito token service.
   *
   * @var \Drupal\cognito\CognitoTokenInterface
   */
  protected $cognitoToken;

  /**
   * UserLoginForm constructor.
   *
   * @param \Drupal\cognito\Aws\CognitoInterface $cognito
   *   The cognito service.
   * @param \Drupal\cognito\CognitoMessagesInterface $cognitoMessages
   *   The cognito messages service.
   * @param \Drupal\cognito\CognitoFlowManagerInterface $cognitoFlowManager
   *   The cognito flow plugin manager.
   * @param \Drupal\externalauth\ExternalAuthInterface $externalAuth
   *   The external auth service.
   * @param \Drupal\cognito\CognitoTokenInterface $cognitoToken
   *   The cognito token service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function __construct(CognitoInterface $cognito, CognitoMessagesInterface $cognitoMessages, CognitoFlowManagerInterface $cognitoFlowManager, ExternalAuthInterface $externalAuth, CognitoTokenInterface $cognitoToken, EventDispatcherInterface $eventDispatcher) {
    $this->cognito = $cognito;
    $this->cognitoMessages = $cognitoMessages;
    $this->cognitoFlowManager = $cognitoFlowManager;
    $this->externalAuth = $externalAuth;
    $this->eventDispatcher = $eventDispatcher;
    $this->cognitoToken = $cognitoToken;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cognito.aws'),
      $container->get('cognito.messages'),
      $container->get('plugin.manager.cognito.cognito_flow'),
      $container->get('externalauth.externalauth'),
      $container->get('cognito.token'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#size' => 60,
      '#maxlength' => Email::EMAIL_MAX_LENGTH,
      '#description' => $this->t('Enter your email address.'),
      '#required' => TRUE,
      '#attributes' => [
        'autocorrect' => 'none',
        'autocapitalize' => 'none',
        'spellcheck' => 'false',
        'autofocus' => 'autofocus',
      ],
    ];

    $form['pass'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#size' => 60,
      '#description' => $this->t('Enter the password that accompanies your username.'),
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Log in')];

    $form['#validate'][] = '::validateAuthentication';

    return $form;
  }

  /**
   * Validate authentication against Cognito.
   */
  public function validateAuthentication(array &$form, FormStateInterface $form_state) {
    $mail = strtolower($form_state->getValue('mail'));
    $password = trim($form_state->getValue('pass'));

    // We validate the users local status as well because it's possible in the
    // future, status updates will be async but we want it to take immediate
    // effect locally.
    if (user_is_blocked($mail)) {
      $form_state->setErrorByName('mail', $this->cognitoMessages->accountBlocked());
      return;
    }

    $errors = $form_state->getErrors();
    if ($errors) {
      return;
    }

    $result = $this->cognito->authorize($mail, $password);

    if ($result->isChallenge()) {
      $flow = $this->cognitoFlowManager->getSelectedFlow();
      $route = $flow->getChallengeRoute($result->getResult()['ChallengeName']);
      $form_state->setRedirect($route);
    }
    elseif ($result->hasError()) {
      if ($result->getErrorCode() === 'PasswordResetRequiredException') {
        $form_state->setErrorByName(NULL, new FormattableMarkup($this->cognitoMessages->passwordResetRequired(), []));
      }
      else {
        $form_state->setErrorByName(NULL, $result->getError());
      }
    }

    // Store authentication result into $form_state for later use.
    if ($result instanceof CognitoResult && isset($result->getResult()['AuthenticationResult'])) {
      $form_state->set('authentication_result', $result->getResult()['AuthenticationResult']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->externalAuth->login($form_state->getValue('mail'), 'cognito');
    $event = new GenericEvent(NULL, [
      'login-context' => \Drupal::request()->get('cognito-login-context') ?: [],
    ]);
    $this->eventDispatcher->dispatch('cognito.logged_in', $event);

    // Store authentication tokens.
    if ($form_state->get('authentication_result')) {
      $this->cognitoToken->setAuthTokens($form_state->get('authentication_result'));
    }
  }

}
