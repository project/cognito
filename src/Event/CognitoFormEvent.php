<?php

namespace Drupal\cognito\Event;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines an event for when Cognito forms are submitted.
 */
class CognitoFormEvent extends Event {

  /**
   * The form state interface.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;

  /**
   * An array of user attributes to be sent to Cognito, in the following format.
   *
   * @var array
   * @code
   * [
   *   'Name' => $fieldName,
   *   'Value => $fieldValue,
   * ]
   * @endcode
   *
   * @See https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-cognito-idp-2016-04-18.html#shape-attributetype
   */
  protected $userAttributes;

  /**
   * CognitoFormEvent constructor.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The formState from the form being submitted.
   * @param array $userAttributes
   *   (Optional) Create the event with some user attributes.
   */
  public function __construct(FormStateInterface $formState, array $userAttributes = []) {
    $this->formState = $formState;
    $this->userAttributes = $userAttributes;
  }

  /**
   * Gets the formState object.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The formState object.
   */
  public function getFormState() {
    return $this->formState;
  }

  /**
   * Adds a user attribute, stored in the correct format for AWS.
   *
   * @param string $name
   *   The name of the attribute.
   * @param string $value
   *   The attribute value.
   *
   * @return $this
   */
  public function addUserAttribute($name, $value) {
    $this->userAttributes[] = [
      'Name' => $name,
      'Value' => $value,
    ];
    return $this;
  }

  /**
   * Gets the current user attributes stored on the vent.
   *
   * @return array[]
   *   The current user attributes array.
   */
  public function getUserAttributes() {
    return $this->userAttributes;
  }

}
