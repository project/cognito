<?php

namespace Drupal\cognito\Plugin\rest\resource;

use Drupal\cognito\Aws\CognitoInterface;
use Drupal\cognito\Aws\CognitoResult;
use Drupal\cognito\CognitoMessagesInterface;
use Drupal\cognito\CognitoTokenInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\Plugin\rest\resource\EntityResourceAccessTrait;
use Drupal\rest\Plugin\rest\resource\EntityResourceValidationTrait;
use Drupal\user\UserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Represents user registration as a resource.
 *
 * @RestResource(
 *   id = "cognito_user_registration",
 *   label = @Translation("Cognito User registration"),
 *   serialization_class = "Drupal\user\Entity\User",
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/cognito/user/register",
 *   },
 * )
 */
class CognitoUserRegistrationResource extends ResourceBase {

  use EntityResourceValidationTrait;
  use EntityResourceAccessTrait;

  /**
   * User settings config instance.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $userSettings;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The cognito service.
   *
   * @var \Drupal\cognito\Aws\Cognito
   */
  protected $cognito;

  /**
   * The Cognito messages service.
   *
   * @var \Drupal\cognito\CognitoMessagesInterface
   */
  protected $cognitoMessages;

  /**
   * The external auth service.
   *
   * @var \Drupal\externalauth\ExternalAuthInterface
   */
  protected $externalAuth;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Cognito token.
   *
   * @var \Drupal\cognito\CognitoTokenInterface
   */
  protected $cognitoToken;

  /**
   * Constructs a new UserRegistrationResource instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ImmutableConfig $user_settings
   *   A user settings config instance.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\cognito\Aws\CognitoInterface $cognito
   *   The Cognito service.
   * @param \Drupal\cognito\CognitoTokenInterface $cognitoToken
   *   The Cognito token.
   * @param \Drupal\cognito\CognitoMessagesInterface $cognitoMessages
   *   The messages service.
   * @param \Drupal\externalauth\ExternalAuthInterface $external_auth
   *   The external auth service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, ImmutableConfig $user_settings, AccountInterface $current_user, CognitoInterface $cognito, CognitoTokenInterface $cognitoToken, CognitoMessagesInterface $cognitoMessages, ExternalAuthInterface $external_auth, EventDispatcherInterface $event_dispatcher, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->userSettings = $user_settings;
    $this->currentUser = $current_user;
    $this->cognito = $cognito;
    $this->cognitoMessages = $cognitoMessages;
    $this->externalAuth = $external_auth;
    $this->eventDispatcher = $event_dispatcher;
    $this->configFactory = $configFactory;
    $this->cognitoToken = $cognitoToken;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('config.factory')->get('user.settings'),
      $container->get('current_user'),
      $container->get('cognito.aws'),
      $container->get('cognito.token'),
      $container->get('cognito.messages'),
      $container->get('externalauth.externalauth'),
      $container->get('event_dispatcher'),
      $container->get('config.factory')
    );
  }

  /**
   * Responds to user registration POST request.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account entity.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   */
  public function post(UserInterface $account = NULL) {
    $this->ensureAccountCanRegister($account);

    // Ensure they can edit all fields that are populated on the entity.
    $this->checkEditFieldAccess($account);

    $email = strtolower($account->getEmail());
    $password = trim($account->getPassword());

    $result = $this->cognito->signUp($email, $password, $email);
    if ($result->hasError()) {
      $errorMessage = $result->getError();

      // If the user already exists then say so.
      if ($result->getErrorCode() === 'UsernameExistsException') {
        // First try and resend the confirmation email, if that fails then
        // display the user not exists exception.
        $resendResult = $this->cognito->resendConfirmationCode($email);
        if (!$resendResult->hasError()) {
          $errorMessage = $this->cognitoMessages->userAlreadyExistsRegister();
        }
      }

      // Registration failed.
      throw new UnprocessableEntityHttpException($errorMessage);
    }

    $accountValues = [];
    foreach ($account->_restSubmittedFields as $key => $field_name) {
      $accountValues[$field_name] = $account->get($field_name)->value;
    }

    // Register the user.
    $registeredAccount = $this->externalAuth->register($email, 'cognito', $accountValues, [
      'frontend_registration' => TRUE, 'api_registration' => TRUE
    ]);

    $context = \Drupal::request()->get('cognito-register-context');
    $event = new GenericEvent(NULL, [
      'register-context' => $context,
      'uid' => $registeredAccount->id(),
      'url' => Url::fromRoute('<front>'),
    ]);

    if ($this->usesAutoConfirm()) {
      $this->externalAuth->login($email, 'cognito');
      // Authorize to start cognito session and obtain auth tokens.
      $result = $this->cognito->authorize($email, $password);
      if ($result instanceof CognitoResult && isset($result->getResult()['AuthenticationResult'])) {
        $this->cognitoToken->setAuthTokens($result->getResult()['AuthenticationResult']);
      }
      $this->eventDispatcher->dispatch('cognito.registered_logged_in', $event);
    }
    else {
      $this->eventDispatcher->dispatch('cognito.registered_click_to_confirm', $event);
    }

    return new ModifiedResourceResponse([
      'registered_user' => [
        'uid' => $registeredAccount->id(),
      ],
      'redirect_url' => $event->getArgument('url')->toString(),
    ], 200);
  }

  /**
   * Ensure the account can be registered in this request.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account to register.
   */
  protected function ensureAccountCanRegister(UserInterface $account = NULL) {
    if ($account === NULL) {
      throw new BadRequestHttpException('No user account data for registration received.');
    }

    // POSTed user accounts must not have an ID set, because we always want to
    // create new entities here.
    if (!$account->isNew()) {
      throw new BadRequestHttpException('An ID has been set and only new user accounts can be registered.');
    }

    // Only allow anonymous users to register, authenticated users with the
    // necessary permissions can POST a new user to the "user" REST resource.
    // @see \Drupal\rest\Plugin\rest\resource\EntityResource
    if (!$this->currentUser->isAnonymous()) {
      throw new AccessDeniedHttpException('Only anonymous users can register a user.');
    }

    // Verify that the current user can register a user account.
    if ($this->userSettings->get('register') == UserInterface::REGISTER_ADMINISTRATORS_ONLY) {
      throw new AccessDeniedHttpException('You cannot register a new user account.');
    }
  }

  /**
   * Sends email notifications if necessary for user that was registered.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account.
   */
  protected function sendEmailNotifications(UserInterface $account) {
    $approval_settings = $this->userSettings->get('register');
    // No e-mail verification is required. Activating the user.
    if ($approval_settings == UserInterface::REGISTER_VISITORS) {
      if ($this->userSettings->get('verify_mail')) {
        // No administrator approval required.
        _user_mail_notify('register_no_approval_required', $account);
      }
    }
    // Administrator approval required.
    elseif ($approval_settings == UserInterface::REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) {
      _user_mail_notify('register_pending_approval', $account);
    }
  }

  /**
   * Checks if we are using auto confirm.
   *
   * This options allows users to register without having to validate their
   * email or phone.
   * It requires a Lambda function to be setup to autoconfirm the users email
   * on registration.
   *
   * @return bool
   *   TRUE if we are using auto confirm otherwise FALSE.
   *
   * @see https://docs.aws.amazon.com/cognito/latest/developerguide/user-pool-lambda-pre-sign-up.html#aws-lambda-triggers-pre-registration-example-2
   */
  protected function usesAutoConfirm() {
    return (bool) $this->configFactory->get('cognito.settings')->get('auto_confirm_enabled');
  }

}
