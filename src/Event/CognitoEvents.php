<?php

namespace Drupal\cognito\Event;

/**
 * Defines events for the cognito module.
 *
 * @see \Drupal\cognito\Event\CognitoFormEvent
 */
final class CognitoEvents {

  /**
   * The name of the event triggered when a user submits a registration.
   *
   * This event allows modules to react before a new user is registered with
   * Cognito.  It gives the ability to add user attributes that should be
   * included in the Congito request.
   *
   * @Event
   *
   * @var string
   */
  const REGISTER = 'event.register';

}
