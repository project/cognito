<?php

namespace Drupal\Tests\cognito\Unit;

use Drupal\cognito\Aws\CognitoInterface;
use Drupal\cognito\CognitoToken;
use Drupal\cognito\CognitoTokenInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserDataInterface;

/**
 * Unit test for cognito token service.
 *
 * @group cognito
 */
class CognitoTokenTest extends UnitTestCase {

  /**
   * The user data service mock object.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * The cognito service mock object.
   *
   * @var \Drupal\cognito\Aws\CognitoInterface
   */
  protected $cognito;

  /**
   * The current user service mock object.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The time service mock object.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The cognito token service mock object.
   *
   * @var \Drupal\cognito\CognitoTokenInterface
   */
  protected $cognitoToken;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->userData = $this->createMock(UserDataInterface::class);
    $this->cognito = $this->createMock(CognitoInterface::class);
    $this->currentUser = $this->createMock(AccountProxyInterface::class);
    $this->time = $this->createMock(TimeInterface::class);
    $this->cognitoToken = new CognitoToken($this->userData, $this->cognito, $this->currentUser, $this->time);
  }

  /**
   * Test the returned data type of setAuthTokens() method.
   */
  public function testSetAuthTokens() {
    $result = $this->cognitoToken->setAuthTokens([]);
    $this->assertInstanceOf(CognitoTokenInterface::class, $result);
  }

  /**
   * Test the getAccessToken() method with no current user.
   */
  public function testGetAccessToken() {
    $result = $this->cognitoToken->getAccessToken();
    $this->assertNull($result);
  }

  /**
   * Test the adminGetAccessToken() method with non-existing user.
   */
  public function testAdminGetAccessToken() {
    $result = $this->cognitoToken->adminGetAccessToken(1234);
    $this->assertNull($result);
  }

  /**
   * Test the returned data type of adminDeleteAuthTokens() method.
   */
  public function testAdminDeleteAuthTokens() {
    $result = $this->cognitoToken->adminDeleteAuthTokens(1234);
    $this->assertInstanceOf(CognitoTokenInterface::class, $result);
  }

}
