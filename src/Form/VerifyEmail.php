<?php

namespace Drupal\cognito\Form;

use Drupal\cognito\Aws\CognitoInterface;
use Drupal\cognito\CognitoTokenInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Email;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class VerifyEmail extends FormBase {

  /**
   * Track whether we're showing the confirmation form.
   *
   * @var bool
   */
  protected $showConfirmation = FALSE;

  /**
   * Keep track of values between form steps.
   *
   * @var array
   */
  protected $multistepFormValues = [];

  /**
   * The cognito service.
   *
   * @var \Drupal\cognito\Aws\Cognito
   */
  protected $cognito;

  /**
   * The Cognito token.
   *
   * @var \Drupal\cognito\CognitoTokenInterface
   */
  protected $cognitoToken;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * PassResetForm constructor.
   *
   * @param \Drupal\cognito\Aws\CognitoInterface $cognito
   *   The cognito service.
   * @param \Drupal\cognito\CognitoTokenInterface $cognitoToken
   *   The cognito token.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function __construct(CognitoInterface $cognito, CognitoTokenInterface $cognitoToken, EventDispatcherInterface $eventDispatcher) {
    $this->cognito = $cognito;
    $this->cognitoToken = $cognitoToken;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cognito.aws'),
      $container->get('cognito.token'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cognito_verify_email';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($this->showConfirmation) {
      return $this->buildConfirmationForm($form);
    }

    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#disabled' => TRUE,
      '#size' => 60,
      '#maxlength' => Email::EMAIL_MAX_LENGTH,
      '#default_value' => $this->currentUser()->isAuthenticated() ? $this->currentUser()->getEmail() : '',
      '#required' => TRUE,
      '#description' => $this->t('Your email address will be verified'),
      '#attributes' => [
        'autocorrect' => 'off',
        'autocapitalize' => 'off',
        'spellcheck' => 'false',
        'autofocus' => 'autofocus',
      ],
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Verify Email')];

    $form['#validate'][] = '::verifyEmailAddress';
    return $form;
  }

  /**
   * Builds the confirmation step form.
   *
   * @param array $form
   *   The form we're adding to.
   *
   * @return array
   *   The form array.
   */
  protected function buildConfirmationForm(array $form) {
    $form['#title'] = $this->t('Please Check your email for the confirmation code');

    // Hide the email form.
    $form['mail']['#access'] = FALSE;

    $form['confirmation_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Confirmation code'),
      '#description' => $this->t('This code has been emailed to your provided email address.'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Confirm'),
    ];

    $form['#validate'][] = '::validateConfirmation';
    return $form;
  }

  /**
   * Sends the verification email for the email attribute.
   */
  public function verifyEmailAddress(array &$form, FormStateInterface $form_state) {
    $accessToken = $this->cognitoToken->getAccessToken();
    $result = $this->cognito->getUserAttributeVerificationCode('email', $accessToken);

    if ($result->hasError()) {
      $form_state->setErrorByName(NULL, $result->getError());
    }
    else {
      $this->showConfirmationStep($form_state);
      $this->messenger()->addMessage($this->t('Please check your email for a verification code.'));
    }
  }

  /**
   * Validate the confirmation form.
   */
  public function validateConfirmation(array &$form, FormStateInterface $form_state) {
    $result = $this->cognito->verifyUserAttribute('email', $form_state->getValue('confirmation_code'), $this->cognitoToken->getAccessToken());

    if ($result->hasError()) {
      $form_state->setErrorByName('confirmation_code', $result->getError());
    }
    else {
      $this->getLogger('cognito')->notice('Your email has now been verified');
      $this->eventDispatcher->dispatch('cognito.email_verified', new GenericEvent());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addMessage($this->t('Your email has now been verified.'));
    $form_state->setRedirect('<front>');
  }

  /**
   * Show the confirmations step of the form.
   *
   * This method handles saving any submitted values and rebuilding the form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  protected function showConfirmationStep(FormStateInterface $form_state) {
    $this->showConfirmation = TRUE;
    $form_state->setRebuild();

    $this->multistepFormValues = $form_state->getValues() + $this->multistepFormValues;
  }

}
