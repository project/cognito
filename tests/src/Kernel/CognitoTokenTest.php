<?php

namespace Drupal\Tests\cognito\Kernel;

use Drupal\cognito\Aws\CognitoInterface;
use Drupal\cognito\Aws\CognitoResult;
use Drupal\cognito\CognitoToken;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Kernel test for cognito token service.
 *
 * @group cognito
 */
class CognitoTokenTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = ['cognito', 'user'];

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->installSchema('user', ['users_data']);
    $this->userData = $this->container->get('user.data');
    $this->currentUser = $this->container->get('current_user');
    $this->time = $this->container->get('datetime.time');
    $this->setUpCurrentUser();
  }

  /**
   * Test getAccessToken() method with active access token.
   */
  public function testGetAccessToken() {
    $cognito = $this->createMock(CognitoInterface::class);
    $cognitoToken = new CognitoToken($this->userData, $cognito, $this->currentUser, $this->time);
    $authenticationResult = [
      'AccessToken' => 'access-token',
      'ExpiresIn' => 1234,
      'TokenType' => 'token-type',
      'IdToken' => 'id-token',
      'RefreshToken' => 'refresh-token',
    ];
    $cognitoToken->setAuthTokens($authenticationResult);
    $result = $cognitoToken->getAccessToken();
    $this->assertEquals('access-token', $result);
  }

  /**
   * Test getAccessToken() method with expired access token.
   */
  public function testGetAccessTokenRefreshed() {
    $cognito = $this->createMock(CognitoInterface::class);
    $cognito
      ->method('refreshAccessToken')
      ->willReturn(new CognitoResult([
        'AuthenticationResult' => [
          'AccessToken' => 'refreshed-access-token',
          'ExpiresIn' => 1234,
          'TokenType' => 'token-type',
          'IdToken' => 'id-token',
        ],
      ]));
    $cognitoToken = new CognitoToken($this->userData, $cognito, $this->currentUser, $this->time);
    // Set expired token with negative timestamp.
    $authenticationResult = [
      'AccessToken' => 'access-token',
      'ExpiresIn' => -1234,
      'TokenType' => 'token-type',
      'IdToken' => 'id-token',
      'RefreshToken' => 'refresh-token',
    ];
    $cognitoToken->setAuthTokens($authenticationResult);
    $result = $cognitoToken->getAccessToken();
    $this->assertEquals('refreshed-access-token', $result);
  }

  /**
   * Test adminGetAccessToken() on current test account.
   */
  public function testAdminGetAccessToken() {
    $cognito = $this->createMock(CognitoInterface::class);
    $cognitoToken = new CognitoToken($this->userData, $cognito, $this->currentUser, $this->time);
    $authenticationResult = [
      'AccessToken' => 'access-token',
      'ExpiresIn' => 1234,
      'TokenType' => 'token-type',
      'IdToken' => 'id-token',
      'RefreshToken' => 'refresh-token',
    ];
    $cognitoToken->setAuthTokens($authenticationResult);
    $result = $cognitoToken->adminGetAccessToken($this->currentUser->id());
    $this->assertEquals('access-token', $result);
  }

  /**
   * Test adminDeleteAuthTokens() on current test account.
   */
  public function testAdminDeleteAuthTokens() {
    $cognito = $this->createMock(CognitoInterface::class);
    $cognitoToken = new CognitoToken($this->userData, $cognito, $this->currentUser, $this->time);
    $authenticationResult = [
      'AccessToken' => 'access-token',
      'ExpiresIn' => 1234,
      'TokenType' => 'token-type',
      'IdToken' => 'id-token',
      'RefreshToken' => 'refresh-token',
    ];
    $cognitoToken->setAuthTokens($authenticationResult);
    $cognitoToken->adminDeleteAuthTokens($this->currentUser->id());
    $result = $cognitoToken->getAccessToken();
    $this->assertNull($result);
  }

}
